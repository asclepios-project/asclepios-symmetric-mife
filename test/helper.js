const {
  openBrowser,
  goto,
  write,
  press,
  setConfig,
  waitFor,
  click,
  text,
  checkBox,
  fileField,
  attach,
  clear,
  $,
} = require("taiko");

const path = require("path");

const login = async (username = "suite5user", password = "suite5") => {
  if (
    (await text("Anonymous").exists()) ||
    (await text("Login", 2000).exists())
  ) {
    await click("Login");
    // await waitFor(5000);
    if (await text("Username or email").exists()) {
      await waitFor("Username or email", 15000);
      await write(username, "Username or email");
      await write(password, "Password");
      await press("Enter");
    }
  }
};

const initBrowser = async () => {
  await openBrowser({
    args: [
      "--window-size=1920,1080",
      "--disable-gpu",
      "--disable-dev-shm-usage",
      "--disable-setuid-sandbox",
      "--no-first-run",
      "--no-sandbox",
      //   "--no-zygote",
    ],
    headless: true,
    // headless: false,
  });
  await setConfig({
    navigationTimeout: 120000,
    observe: true,
    observeTime: 3000,
    retryTimeout: 5000,
  });

  //   await waitFor(2000);
  await goto("http://localhost");
  await waitFor(1000);
};

// const uploadFe = async (
//   keyId,
//   passphrase,
//   fileIDs,
//   filename = "user-data.json",
//   fieldList = ["height", "date1", "date2", "age"]
// ) => {
//   await write(
//     passphrase,
//     "Passphrase (it will be hashed prior to sending to Trusted Authority)"
//   );
//   await write(keyId, "Key ID");
//   await waitFor(1000);
//   await click("Send");
//   await attach(path.join(__dirname, filename), fileField({ id: "jsonFile" }));
//   //   await attach("./user-data.json", $("#jsonFile"));
//   await write(fileIDs, "Unique file id");
//   await write(passphrase, $("#passphrase1"));
//   await write(keyId, $("#keyid1"));
//   await checkBox("height", { selectHiddenElements: true }).exists();
//   await checkBox({ id: "fe-height" }, { selectHiddenElements: true }).check();
//   await checkBox({ id: "fe-date1" }, { selectHiddenElements: true }).check();
//   await checkBox({ id: "fe-date2" }, { selectHiddenElements: true }).check();
//   await checkBox({ id: "fe-age" }, { selectHiddenElements: true }).check();
//   await click($("#btnSubmitFile"));
// };

// const uploadFe = async (
//   keyId,
//   passphrase,
//   fileIDs,
//   filename = "user-data.json",
//   fieldList = ["height", "date1", "date2", "age"]
// ) => {
//   await write(
//     passphrase,
//     "Passphrase (it will be hashed prior to sending to Trusted Authority)"
//   );
//   await write(keyId, "Key ID");
//   await waitFor(1000);
//   await click("Send");
//   await attach(path.join(__dirname, filename), fileField({ id: "jsonFile" }));
//   //   await attach("./user-data.json", $("#jsonFile"));
//   await write(fileIDs, "Unique file id");
//   await write(passphrase, $("#passphrase1"));
//   await write(keyId, $("#keyid1"));
//   await checkBox("height", { selectHiddenElements: true }).exists();
//   await checkBox({ id: "fe-height" }, { selectHiddenElements: true }).check();
//   await checkBox({ id: "fe-date1" }, { selectHiddenElements: true }).check();
//   await checkBox({ id: "fe-date2" }, { selectHiddenElements: true }).check();
//   await checkBox({ id: "fe-age" }, { selectHiddenElements: true }).check();
//   await click($("#btnSubmitFile"));
// };

const uploadFe = async (
  keyId,
  passphrase,
  fileIDs,
  filename = "user-data.json",
  fieldList = ["height", "date1", "date2", "age"]
) => {
  passphrase;

  await clear($("#passphrase"));
  await write(passphrase, $("#passphrase"));

  await clear($("#keyid"));
  await write(keyId, $("#keyid"));

  await waitFor(1000);
  await click("Send");

  await clear($("#fileid1"));
  await clear($("#passphrase1"));
  await clear($("#keyid1"));

  await waitFor(1000);

  await attach(path.join(__dirname, filename), fileField({ id: "jsonFile" }));
  //   await attach("./user-data.json", $("#jsonFile"));

  await write(fileIDs, $("#fileid1"));

  await write(passphrase, $("#passphrase1"));

  await write(keyId, $("#keyid1"));

  waitFor(2000);
  await fieldList.forEach(async (field) => {
    await checkBox(
      { id: `fe-${field}` },
      { selectHiddenElements: true }
    ).check();
  });
  await click($("#btnSubmitFile"));
};

// fieldList.forEach(field => {
//   await checkBox({ id: `fe-${field}` }, { selectHiddenElements: true }).check();
// });

// await checkBox("height", { selectHiddenElements: true }).exists();

// await checkBox({ id: "fe-height" }, { selectHiddenElements: true }).check();
// await checkBox({ id: "fe-date1" }, { selectHiddenElements: true }).check();
// await checkBox({ id: "fe-date2" }, { selectHiddenElements: true }).check();
// await checkBox({ id: "fe-age" }, { selectHiddenElements: true }).check();
// await click($("#btnSubmitFile"));

module.exports = { initBrowser, login, uploadFe };
