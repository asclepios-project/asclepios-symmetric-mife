const {
  closeBrowser,
  write,
  waitFor,
  click,
  text,
  attach,
  fileField,
  dropDown,
  below,
  $,
} = require("taiko");

const { initBrowser, login, uploadFe } = require("./helper.js");

const path = require("path");

describe("Taiko with Jest", () => {
  jest.setTimeout(600000);

  const keyId1 = "5";
  const passphrase = "12345";

  beforeAll(async () => {
    await initBrowser();
    await login();
    await uploadFe(keyId1, passphrase, "11,22,33,44,55", "user-data2.json");
  });

  describe("FE-SSE integration E2E Testing Basic 5", () => {
    test("1) Login user1 => Upload Data => Logout user1 => Login user2 => Compute sum  with SSE => Logout", async () => {
      await click("Logout");
      await login("suite5user2", "suite52");
      await waitFor("Logout", 5000);
      await expect(await text("Logout").exists()).toBeTruthy();

      await click(
        "Search",
        { navigationTimeout: 60000 },
        below("FUNCTIONAL ENCRYPTION | ANALYST:")
      );
      await attach(
        path.join(__dirname, "search2.json"),
        fileField({ id: "fe-search-file" })
      );

      await write(passphrase, $("#fe-passphrase"));
      await write(keyId1, $("#fe-keyid"));
      await write("height", $("#fe-analyst-field"));

      await dropDown("Function:").select("sum");
      await click($("#fe-analyst-compute"));

      await waitFor("sum:310", 5000);
      await expect(await text("sum:310").exists()).toBeTruthy();
    }, 600000);

    test("2) Login user2 => delete data of user1", async () => {
      await click("Logout");
      await login("suite5user2", "suite52");
      await waitFor("Logout", 5000);
      await expect(await text("Logout").exists()).toBeTruthy();

      await write("11", $("#fileid3"));
      await write("12345", $("#passphrase4"));
      await write(keyId1, $("#keyid4"));

      await click($("#btnDeleteFile"));
      await expect(
        !(await text(`FE Keys/Values deleted list:${keyId1}#11`).exists())
      ).toBeTruthy();
    }, 600000);

    test("3) Login user2 => update data of user1", async () => {
      await click("Logout");
      await login("suite5user2", "suite52");
      await waitFor("Logout", 5000);
      await expect(await text("Logout").exists()).toBeTruthy();

      await attach(
        path.join(__dirname, "user-data-update.json"),
        fileField({ id: "jsonUpdateFile" })
      );
      await write("11", $("#fileid2"));
      await write("12345", $("#passphrase3"));
      await write(keyId1, $("#keyid3"));

      await click($("#btnUpdateFile"));
      await expect(
        !(await text(`FE Ciphertexts:{"${keyId1}#11@`).exists())
      ).toBeTruthy();
    }, 600000);
  });

  afterAll(async () => {
    await closeBrowser();
  });
});
