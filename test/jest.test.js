const {
  openBrowser,
  goto,
  closeBrowser,
  write,
  press,
  setConfig,
  waitFor,
  click,
  text,
  attach,
  fileField,
  checkBox,
  dropDown,
  below,
  screenshot,
  clear,
  $,
} = require("taiko");

const { initBrowser, login, uploadFe } = require("./helper.js");

const path = require("path");

describe("Taiko with Jest", () => {
  jest.setTimeout(200000);

  const keyId = "1";
  beforeAll(async () => {
    await initBrowser();
    await login();
  });

  describe("FE-SSE integration E2E Testing Basic 1", () => {
    test("1) Login", async () => {
      await expect(await text("Logout").exists()).toBeTruthy();
    }, 600000);

    test('2) Upload"', async () => {
      await uploadFe(keyId, "12345", "11,22");
      await waitFor(2000);

      await expect(await text("FE Ciphertexts:").exists()).toBeTruthy();
      await expect(await text("1#").exists()).toBeTruthy();
    });

    test('3) Sum: age1:40 + age2:40"', async () => {
      await clear($("#fe-keyid"));
      await clear($("#fe-analyst-field"));
      await clear($("#fe-analyst-fileIDs"));

      await write("11,22", $("#fe-analyst-fileIDs"));
      await write(keyId, $("#fe-keyid"));
      await write("age", $("#fe-analyst-field"));

      await dropDown("Function:").select("sum");
      await click($("#fe-analyst-compute"));
      await waitFor(2000);

      await expect(await text("sum:80").exists()).toBeTruthy();
    });

    test('4) Average: (age1:40 + age2:40)/2 "', async () => {
      await clear($("#fe-keyid"));
      await clear($("#fe-analyst-field"));
      await clear($("#fe-analyst-fileIDs"));

      await write("11,22", $("#fe-analyst-fileIDs"));
      await write(keyId, $("#fe-keyid"));
      await write("age", $("#fe-analyst-field"));

      await dropDown("Function:").select("average");
      await click($("#fe-analyst-compute"));
      await waitFor(2000);

      await expect(await text("average:40").exists()).toBeTruthy();
    });

    test('5) Subtract: (age1:40 - age2:40)"', async () => {
      await clear($("#fe-keyid"));
      await clear($("#fe-analyst-field"));
      await clear($("#fe-analyst-fileIDs"));

      await write("11,22", $("#fe-analyst-fileIDs"));
      await write(keyId, $("#fe-keyid"));
      await write("age", $("#fe-analyst-field"));

      await dropDown("Function:").select("subtract");
      await click($("#fe-analyst-compute"));
      await waitFor(2000);

      await expect(await text("subtract:0").exists()).toBeTruthy();
    });

    test('6) Subtract with SSE: (height1:140 - height2:150) with search (age == 40)"', async () => {
      await click(
        "Search",
        { navigationTimeout: 60000 },
        below("FUNCTIONAL ENCRYPTION | ANALYST:")
      );
      await attach(
        path.join(__dirname, "search.json"),
        fileField({ id: "fe-search-file" })
      );
      await clear($("#fe-passphrase"));
      await clear($("#fe-keyid"));
      await clear($("#fe-analyst-field"));

      await write("12345", $("#fe-passphrase"));
      await write(keyId, $("#fe-keyid"));
      await write("height", $("#fe-analyst-field"));

      await dropDown("Function:").select("subtract");
      await click($("#fe-analyst-compute"));
      await waitFor(2000);

      await expect(await text("subtract:10").exists()).toBeTruthy();
    });

    test('7) Subtract with SSE: (date2 - date1) with search (age == 40)"', async () => {
      await click(
        "Search",
        { navigationTimeout: 60000 },
        below("FUNCTIONAL ENCRYPTION | ANALYST:")
      );
      await attach(
        path.join(__dirname, "search.json"),
        fileField({ id: "fe-search-file" })
      );
      await clear($("#fe-passphrase"));
      await clear($("#fe-keyid"));
      await clear($("#fe-analyst-field"));

      await write("12345", $("#fe-passphrase"));
      await write(keyId, $("#fe-keyid"));
      await clear($("#fe-analyst-field"));
      await write("date2,date1", $("#fe-analyst-field"));

      await dropDown("Function:").select("subtract");
      await click($("#fe-analyst-compute"));
      await waitFor(2000);

      await expect(await text("subtract:60,60").exists()).toBeTruthy();
    });

    test("8) Update data: date1", async () => {
      await attach(
        path.join(__dirname, "user-data-update.json"),
        fileField({ id: "jsonUpdateFile" })
      );
      await write("11", $("#fileid2"));
      await write("12345", $("#passphrase3"));
      await write(keyId, $("#keyid3"));

      await dropDown("Function:").select("subtract");
      await click($("#btnUpdateFile"));
      // 1#22@eeba8bb6ff8b72caf50fc830af24f316ef358db14e25f0408f9e3e8e612a033e
      await expect(
        await text(`FE Ciphertexts:{"${keyId}#11@`).exists()
      ).toBeTruthy();
      //   await expect(await text("subtract:10").exists()).toBeTruthy();
    });

    test("9) Delete data", async () => {
      await write("11", $("#fileid3"));
      await write("12345", $("#passphrase4"));
      await write(keyId, $("#keyid4"));

      await click($("#btnDeleteFile"));
      // 1#22@eeba8bb6ff8b72caf50fc830af24f316ef358db14e25f0408f9e3e8e612a033e
      await expect(
        await text(`FE Keys/Values deleted list:${keyId}#11`).exists()
      ).toBeTruthy();
    });

    test("10) Logout", async () => {
      await click("Logout");
      await expect(await text("Login").exists()).toBeTruthy();
    });
  });

  afterAll(async () => {
    await closeBrowser();
  });
});
