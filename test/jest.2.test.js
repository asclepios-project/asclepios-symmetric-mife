const {
  closeBrowser,
  write,
  waitFor,
  click,
  text,
  attach,
  fileField,
  checkBox,
  dropDown,
  below,
  $,
  clear,
} = require("taiko");

const { initBrowser } = require("./helper.js");

const path = require("path");

describe("Taiko with Jest", () => {
  jest.setTimeout(200000);

  const keyId = "2";

  beforeAll(async () => {
    await initBrowser();
  });

  describe("FE-SSE integration E2E Testing Basic 2", () => {
    test("1) Not Login ", async () => {
      await expect(await text("Login").exists()).toBeTruthy();
    }, 600000);

    test('2) Upload | Unauthorized request"', async () => {
      await write(
        "12345",
        "Passphrase (it will be hashed prior to sending to Trusted Authority)"
      );
      await write(keyId, "Key ID");
      await waitFor(1000);
      await click("Send");
      await attach(
        path.join(__dirname, "user-data.json"),
        fileField({ id: "jsonFile" })
      );
      //   await attach("./user-data.json", $("#jsonFile"));
      await write("11,22", "Unique file id");
      await write("12345", $("#passphrase1"));
      await write(keyId, $("#keyid1"));
      await checkBox("height", { selectHiddenElements: true }).exists();
      await checkBox(
        { id: "fe-height" },
        { selectHiddenElements: true }
      ).check();
      await checkBox(
        { id: "fe-date1" },
        { selectHiddenElements: true }
      ).check();
      await checkBox(
        { id: "fe-date2" },
        { selectHiddenElements: true }
      ).check();
      await checkBox({ id: "fe-age" }, { selectHiddenElements: true }).check();
      await click($("#btnSubmitFile"));
      await waitFor(2000);

      await expect(!(await text("FE Ciphertexts:").exists())).toBeTruthy();
    });

    test("3) Sum: age1:40 + age2:40 | Unauthorized request", async () => {
      await clear($("#fe-analyst-fileIDs"));
      await write("11,22", $("#fe-analyst-fileIDs"));
      await clear($("#fe-keyid"));
      await write(keyId, $("#fe-keyid"));
      await clear($("#fe-analyst-field"));
      await write("age", $("#fe-analyst-field"));

      await dropDown("Function:").select("sum");
      await click($("#fe-analyst-compute"));
      await waitFor(2000);

      await expect(!(await text("sum:").exists())).toBeTruthy();
    });

    test("4) Subtract with SSE | Unauthorized request", async () => {
      await click(
        "FileIDs",
        { navigationTimeout: 60000 },
        below("FUNCTIONAL ENCRYPTION | ANALYST:")
      );
      // await attach(
      //   path.join(__dirname, "search.json"),
      //   fileField({ id: "fe-search-file" })
      // );

      // await clear($("#fe-passphrase"));
      // await write("12345", $("#fe-passphrase"));

      await clear($("#fe-analyst-fileIDs"));
      await write("1", $("#fe-analyst-fileIDs"));

      await clear($("#fe-keyid"));
      await write(keyId, $("#fe-keyid"));
      await clear($("#fe-analyst-field"));
      await write("height", $("#fe-analyst-field"));

      await dropDown("Function:").select("subtract");
      await click($("#fe-analyst-compute"));
      await waitFor(2000);

      await expect(!(await text("subtract:10").exists())).toBeTruthy();
      await expect(
        await text(
          "Unauthenticated request",
          below("FUNCTIONAL ENCRYPTION | ANALYST:")
        ).exists()
      ).toBeTruthy();
    });

    test("5) Update data: date1 | return nothing", async () => {
      await attach(
        path.join(__dirname, "user-data-update.json"),
        fileField({ id: "jsonUpdateFile" })
      );
      await write("11", $("#fileid2"));
      await write("12345", $("#passphrase3"));
      await write(keyId, $("#keyid3"));

      await dropDown("Function:").select("subtract");
      await click($("#btnUpdateFile"));
      await expect(
        !(await text(`FE Ciphertexts:{"${keyId}#11@`).exists())
      ).toBeTruthy();
    });

    test("6) Delete data | return nothing", async () => {
      await write("11", $("#fileid3"));
      await write("12345", $("#passphrase4"));
      await write(keyId, $("#keyid4"));

      await click($("#btnDeleteFile"));
      await expect(
        !(await text(`FE Keys/Values deleted list:${keyId}#11`).exists())
      ).toBeTruthy();
    });
  });

  afterAll(async () => {
    await closeBrowser();
  });
});
