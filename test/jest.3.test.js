const {
  closeBrowser,
  write,
  waitFor,
  click,
  text,
  attach,
  fileField,
  dropDown,
  below,
  evaluate,
  $,
  reload,
  clear,
} = require("taiko");

const { initBrowser, login, uploadFe } = require("./helper.js");

const path = require("path");

describe("Taiko with Jest", () => {
  jest.setTimeout(600000);

  const keyId1 = "3";
  const keyId2 = "4";
  const passphrase = "12345";

  beforeAll(async () => {
    await initBrowser();
    await login();
    // await uploadFe(keyId1, passphrase, "11,22");
    await uploadFe(keyId1, passphrase, "11,22,33,44,55", "user-data2.json");
    await uploadFe(keyId2, passphrase, "11,22", "user-data.json");
  });

  describe("3. FE-SSE integration E2E Testing Advanced", () => {
    test("1) Should be logged in ", async () => {
      await expect(await text("Logout").exists()).toBeTruthy();
    }, 600000);

    test('2) Sum: age1:40 + age2:40 + age3:40 + age4:50 + age5:50"', async () => {
      await reload();
      // await waitFor(1000);

      await click(
        "FileIDs",
        { navigationTimeout: 60000 },
        below("FUNCTIONAL ENCRYPTION | ANALYST:")
      );

      await write("11,22,33,44,55", $("#fe-analyst-fileIDs"));

      await write(keyId1, $("#fe-keyid"));

      await write("age", $("#fe-analyst-field"));

      await dropDown("Function:").select("sum");

      await click($("#fe-analyst-compute"));

      await waitFor("sum:220", 5000);
      await expect(await text("sum:220").exists()).toBeTruthy();
    });

    test("3) Sum with SSE search  for age === 50 ", async () => {
      await reload();
      await click(
        "Search",
        { navigationTimeout: 60000 },
        below("FUNCTIONAL ENCRYPTION | ANALYST:")
      );
      await attach(
        path.join(__dirname, "search2.json"),
        fileField({ id: "fe-search-file" })
      );

      await write(passphrase, $("#fe-passphrase"));
      await write(keyId1, $("#fe-keyid"));
      await write("height", $("#fe-analyst-field"));

      await dropDown("Function:").select("sum");
      await click($("#fe-analyst-compute"));

      await waitFor("sum:310", 5000);
      await expect(await text("sum:310").exists()).toBeTruthy();
    });
  });

  describe("4. FE-SSE integration E2E Testing Advanced", () => {
    test("1) Subtract with SSE | ", async () => {
      await reload();
      await click(
        "Search",
        { navigationTimeout: 60000 },
        below("FUNCTIONAL ENCRYPTION | ANALYST:")
      );
      await attach(
        path.join(__dirname, "search.json"),
        fileField({ id: "fe-search-file" })
      );
      await write(passphrase, $("#fe-passphrase"));
      await write(keyId1, $("#fe-keyid"));
      await dropDown("Function:").select("subtract");
    });

    test("2) Subtract with SSE | MORE than 2 fileIDs and 1 field", async () => {
      await write("height", $("#fe-analyst-field"));
      await click($("#fe-analyst-compute"));

      let content = await evaluate(
        $("#notify-analyst > .alert:nth-child(1)"),
        (element) => element.innerText
      );
      console.log(content);
      await expect(
        content ===
          "Subtract needs at least 1 file ID and 2 fields or 2 file IDs and 1 field"
      ).toBeTruthy();
    });

    test("3) Subtract with SSE | more than 2 fileIDs and 2 fields", async () => {
      await clear($("#fe-analyst-field"));
      await write("date2,date1", $("#fe-analyst-field"));

      await click($("#fe-analyst-compute"));

      await waitFor("subtract:60,60,60", 5000);
      await expect(await text("subtract:60,60,60").exists()).toBeTruthy();
    });

    test("4) Subtract with SSE | 2 fileIDs and 1 field", async () => {
      await clear($("#fe-analyst-field"));

      await attach(
        path.join(__dirname, "search2.json"),
        fileField({ id: "fe-search-file" })
      );
      await write("date1", $("#fe-analyst-field"));

      await click($("#fe-analyst-compute"));

      await waitFor("subtract:3600", 5000);
      await expect(await text("subtract:3600").exists()).toBeTruthy();
    });
  });

  afterAll(async () => {
    await closeBrowser();
  });
});
