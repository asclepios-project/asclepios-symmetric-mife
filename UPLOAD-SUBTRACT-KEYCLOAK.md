# ASCLEPIOS Symmetric MIFE | SSE-FE

## SSE prerequisite:

1. SSE Passphrase: 123456
2. SSE key: 2

## Keycloack tokens:

### Obtain Token and Refresh-Token from keycloak:

You can use our enpoint or any other method in order to obtain the tokens

```bash
curl -XPOST 'http://localhost:5010/login'-H 'Content-Type: application/json' --data-raw '{"username":"suite5user2","password":"suite5user2"}' 
```

## Uploading data with FE

### Data to encrypt via FE

```json
[
  {
    "name": "name1",
    "height": 140,
    "date1": "09/12/2017 12:11:11",
    "date2": "09/12/2017 12:12:11",
    "age": 40
  },
  {
    "name": "name2",
    "height": 150,
    "date1": "09/12/2017 12:11:10",
    "date2": "09/12/2017 12:12:10",
    "age": 40
  }
]
```

### Send Request via CURL to obtain fe keys

Each mapping key is in the form:  
<sse_key> # <file_id> @ sha256(<field_name>)

sse_key: Obtain from SSE if exists

file_id: Can be any unique number

field_name: sha256("height") or "height"

ex.:

"2#44@39e0f5efdc39ec10992833ad019f0ddf2b42b49b098313df991b8229a37aed21"

2 # 44 @ 39e0f5efdc39ec10992833ad019f0ddf2b42b49b098313df991b8229a37aed21

### Request FE keys from TA

```bash
curl 'http://localhost:5010/get-keys'
-H 'Token: <token>'
-H 'Content-Type: application/json'
--data-raw '["2#44@39e0f5efdc39ec10992833ad019f0ddf2b42b49b098313df991b8229a37aed21","2#44@eeba8bb6ff8b72caf50fc830af24f316ef358db14e25f0408f9e3e8e612a033e","2#44@367ef122402d924b435d48519bdcd8359aad934817a0a551bb44bbc5c6ab41c6","2#44@013f54400c82da08037759ada907a8b864e97de81c088a182062c4b5622fd2ab","2#33@39e0f5efdc39ec10992833ad019f0ddf2b42b49b098313df991b8229a37aed21","2#33@eeba8bb6ff8b72caf50fc830af24f316ef358db14e25f0408f9e3e8e612a033e","2#33@367ef122402d924b435d48519bdcd8359aad934817a0a551bb44bbc5c6ab41c6","2#33@013f54400c82da08037759ada907a8b864e97de81c088a182062c4b5622fd2ab"]'
```

### Response:

```json
{
  "code": 200,
  "results": {
    "feKeys": {
      "2#44@39e0f5efdc39ec10992833ad019f0ddf2b42b49b098313df991b8229a37aed21": 6859661,
      "2#44@eeba8bb6ff8b72caf50fc830af24f316ef358db14e25f0408f9e3e8e612a033e": 9751296,
      "2#44@367ef122402d924b435d48519bdcd8359aad934817a0a551bb44bbc5c6ab41c6": 8080945,
      "2#44@013f54400c82da08037759ada907a8b864e97de81c088a182062c4b5622fd2ab": 698725,
      "2#33@39e0f5efdc39ec10992833ad019f0ddf2b42b49b098313df991b8229a37aed21": 5520428,
      "2#33@eeba8bb6ff8b72caf50fc830af24f316ef358db14e25f0408f9e3e8e612a033e": 792554,
      "2#33@367ef122402d924b435d48519bdcd8359aad934817a0a551bb44bbc5c6ab41c6": 5646968,
      "2#33@013f54400c82da08037759ada907a8b864e97de81c088a182062c4b5622fd2ab": 2051387
    },
    "fileIDsExists": []
  }
}
```

### Send encrypted data base on above data:

THe encryption preparation is in the client side:

<raw value> + <fe-key> = <encrypted value>

ex.
140 + 6859661 = 6859801

#### Request:

```bash
curl 'http://localhost:5011/ciphertexts' -H 'Content-Type: application/json' -H 'Token: <token>' --data-raw '{"2#44@39e0f5efdc39ec10992833ad019f0ddf2b42b49b098313df991b8229a37aed21":6859801,"2#44@eeba8bb6ff8b72caf50fc830af24f316ef358db14e25f0408f9e3e8e612a033e":1522565567,"2#44@367ef122402d924b435d48519bdcd8359aad934817a0a551bb44bbc5c6ab41c6":1520895276,"2#44@013f54400c82da08037759ada907a8b864e97de81c088a182062c4b5622fd2ab":698765,"2#33@39e0f5efdc39ec10992833ad019f0ddf2b42b49b098313df991b8229a37aed21":5520578,"2#33@eeba8bb6ff8b72caf50fc830af24f316ef358db14e25f0408f9e3e8e612a033e":1513606824,"2#33@367ef122402d924b435d48519bdcd8359aad934817a0a551bb44bbc5c6ab41c6":1518461298,"2#33@013f54400c82da08037759ada907a8b864e97de81c088a182062c4b5622fd2ab":2051427}'
```

#### Response:

```json
{ "code": 200, "message": "done" }
```

## Computation - Subtract - date2 -date1

### Request subtract from TA - SYNC

#### Request:

```bash
curl 'http://localhost:5010/compute'  -H 'Content-Type: application/json' -H 'Token: <token>' --data-raw '{"fileIDs":["2#33","2#44"],"field":"eeba8bb6ff8b72caf50fc830af24f316ef358db14e25f0408f9e3e8e612a033e,367ef122402d924b435d48519bdcd8359aad934817a0a551bb44bbc5c6ab41c6","function":"subtract"}'
```

#### Response:

```bash
{"code": 200, "token": "ec2167ac-3c8b-4463-b5ba-dbd46af0f11c"}
```

### Request subtract from TA - ASYNC

Append:

```json
 "async": "true"
```

#### Request:

```bash
curl 'http://localhost:5010/compute'  -H 'Content-Type: application/json' -H 'Token: <token>' --data-raw '{"fileIDs":["2#33","2#44"],"field":"eeba8bb6ff8b72caf50fc830af24f316ef358db14e25f0408f9e3e8e612a033e,367ef122402d924b435d48519bdcd8359aad934817a0a551bb44bbc5c6ab41c6","function":"subtract", "async": "true"}'
```

#### Response:

```bash
{"code": 200, "token": "ec2167ac-3c8b-4463-b5ba-dbd46af0f11c"}
```

### Request results from EV

#### Request

```bash
curl 'http://localhost:5011/get-result' -H 'Content-Type: application/json' -H 'Token: <token>' --data-raw '{"token":"ec2167ac-3c8b-4463-b5ba-dbd46af0f11c"}'
```

#### Response - SYNC

```json
{ "code": 200, "results": [-60.0, -60.0] }
```

#### Response - ASYNC

if the computation is in progress

```json
{ "code": 200, "results": "Computation is in progress" }
```

or if the computation is finished.

```json
{ "code": 200, "results": [-60.0, -60.0] }
```
