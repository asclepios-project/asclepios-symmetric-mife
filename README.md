# ASCLEPIOS Symmetric MIFE | SSE-FE

Implementation of the ASCLEPIOS symmetric multi-input functional encryption scheme.

## Contents

1. [FE standalone full run (curl base example)](#fe-standalone-full-run-curl-base-example)
2. [FE standalone HTML/Javascript](./standalone/README.md)
3. [SSE-FE Integration](#sse-fe-integration)
4. [SSE-FE API specification](./SSE_API_specification_with_FE_V1.1.pdf)

## FE standalone full run curl base example

Running the below diagram flow ( TA generates the fe key and stores them in sqlite3 DB with AES encryption)
The below script runs the TA and EV service and then execute all the steps via curl in order to demonstrates a FE flow.

run-full-demo.sh \<fileID1\>:\<age1\>&nbsp;&nbsp;\<fileID2\>:\<age2\> ....

```bash
cd ./asclepios-fe-1/
./run-full-demo.sh 123:30 456:88
```

![TA generates fe keys](./asclepios-fe-1/asclepios-fe-ta-generates-fe-keys.svg)

## SSE-FE Integration

Run the below commands on separate terminals:

### Terminal 1:

Initialize and start the TA service and EV service.

```bash
./run-services.sh
```

### Terminal 2:

Build/Run the sseclient which includes our sse-fe javascript api integration with all the SSE services

```bash
./run-sseclient.sh
```

open via browser the http://localhost:80 do the below steps:

#### a) upload an SSE key

![upload an SSE key](./upload-sse-key.png)

#### b) upload sse data using the user-data.json as input and please be aware to select any of the check box below in order to enable FE on the specific field

[user-data.json](./user-data.json)

![upload SSE-FE data](./upload-sse-fe-data.png)

#### c) update sse data using the user-data-update.json as input, fileID and passphrase.

[user-data-update.json](./user-data-update.json)

![update SSE-FE data](./update-sse-fe-data.png)

#### d) delete sse data using a fileID and passphrase.

![delete SSE-FE data](./delete-sse-fe-data.png)

### Analyst steps:

#### c) Use as input (1)fileIDs, (2)the field on which the computation should be performed and (3)the type of function to be applied .

![compute-SSE-FE-1](./compute-fe-with-fileId-as-input.png)

#### d) Perform a search using the SSE and then apply the requested computation on the files that have the fileIDs returned by the search. In this case you need to provide (1)the search query, (2) the field and (3) the function as before

[search.json](./search.json)

![compute-SSE-FE-2](./compute-fe-with-sse.png)

### Live demos

#### a) Subtract/Update date1 and date2

![Subtract/Update date1,date2](./subtract-update-date1-date2.gif)

#### b) Full demo

![Asclepios CLient - TA- EV - (TA generates fe keys - HTML/Javascript)](./fe-sse-integration.gif)
