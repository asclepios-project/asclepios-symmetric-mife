# -*- coding: utf-8 -*-
import json
import logging
import operator
import re
import subprocess
import sys
from hashlib import sha256
from random import randrange

from flask import Flask
from flask_restful import Api

from keycloak_util import kc_page
from parameters import debug, dev, server_port
from ta_util import TaCompute, TaDelete, TaKeygenerator

app = Flask(__name__)
app.config['SECRET_KEY'] = '37af2c72-53c3-4d8e-bae0-609a5a2934f6'
api = Api(app)

@app.after_request
def add_header(response):
    if dev:
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Headers'] = '*'
    return response


api.add_resource(TaKeygenerator, '/get-keys')
api.add_resource(TaCompute, '/compute')
api.add_resource(TaDelete, '/delete')
app.register_blueprint(kc_page)

if __name__ == '__main__':
    app.run(host= '0.0.0.0',port=server_port,debug=debug)

