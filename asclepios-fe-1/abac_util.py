import logging
import urllib.parse
from json import loads
from string import Template
from uuid import uuid4

import requests

from parameters import abac_api_key, abac_disabled, abac_url, keycloak_issuer

log = logging.getLogger("ABAC-Util")

def CheckAccessRequestTemplate(environ, userinfo):
    try:
        return  loads(
            Template("""{
            "type": "eu.asclepios.authorization.abac.util.CheckAccessRequest",
            "value": {
                ".id": {
                    "type": "java.lang.String",
                    "value": "$_id"
                },
                ".xacmlRequest": {
                    "type": "map",
                    "class": "java.util.HashMap",
                    "value": {
                        "kc-family-name": {
                            "type": "java.lang.String",
                            "value": "$family_name"
                        },
                        "kc-issuer": {
                            "type": "java.lang.String",
                            "value": "$issuer"
                        },
                        "http-req-method": {
                            "type": "java.lang.String",
                            "value": "$REQUEST_METHOD"
                        },
                        "resource-id": {
                            "type": "java.lang.String",
                            "value": "$RAW_URI"
                        },
                        "kc-name": {
                            "type": "java.lang.String",
                            "value": "$name"
                        },
                        "actionId": {
                            "type": "java.lang.String",
                            "value": "$REQUEST_METHOD"
                        },
                        "kc-given-name": {
                            "type": "java.lang.String",
                            "value": "$given_name"
                        },
                        "http-req-url": {
                            "type": "java.lang.String",
                            "value": "$RAW_URI"
                        },
                        "http-req-uri": {
                            "type": "java.lang.String",
                            "value": "$RAW_URI"
                        },
                        "http-req-scheme": {
                            "type": "java.lang.String",
                            "value": "$url_scheme"
                        },
                        "kc-preferred-username": {
                            "type": "java.lang.String",
                            "value": "$preferred_username"
                        },
                        "kc-email": {
                            "type": "java.lang.String",
                            "value": "$email"
                        },
                        "legalName": {
                            "type": "java.lang.String",
                            "value": "test"
                        }
                    }
                }
            }
        }""").safe_substitute(
            {
                **userinfo,
                **environ,
                **{
                    "url_scheme": environ.get("wsgi.url_scheme", ""), 
                    "_id": str(uuid4()),
                    # "REQUEST_URI": urllib.parse.quote_plus(environ.get("REQUEST_URI", "")),
                    "RAW_URI": urllib.parse.quote_plus(environ.get("RAW_URI", "")),
                    "issuer": urllib.parse.quote_plus(keycloak_issuer),
                }
            }
        )
    )
    except Exception as e:
        import sys
        import traceback 
        traceback.print_exception(*sys.exc_info())
        return {}


def permitCheck(environ, userinfo):
    if abac_disabled:
        return True
    data = CheckAccessRequestTemplate(environ, userinfo)
    headers = {'X-API-KEY': abac_api_key}
    response = requests.post(abac_url, json=data, headers=headers, verify=False)
    return "PERMIT" in response.text and data["value"][".id"]["value"] in response.text
