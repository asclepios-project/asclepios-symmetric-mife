#! -*- coding: utf-8 -*-
import base64
import functools
import logging
import time

from flask import (Blueprint, Flask, Response, abort, g, jsonify, redirect,
                   render_template, request, session)
from jinja2 import TemplateNotFound
from keycloak import KeycloakOpenID

from parameters import (keycloak_client_id, keycloak_client_secret_key,
                        keycloak_realm_name, keycloak_server_url)

log = logging.getLogger("Keycloak-Util")

kc_page = Blueprint('kc', __name__,
                        template_folder='templates')



kc = KeycloakOpenID(server_url=keycloak_server_url,
                    client_id=keycloak_client_id,
                    realm_name=keycloak_realm_name,
                    client_secret_key=keycloak_client_secret_key
                )

def getUserInfo():
    token = request.headers.get("Token")
    if token is None:
        g.user = None
        return
    try:
        introspect_user = kc.introspect(token)
        if introspect_user.get("active"):
            g.user = introspect_user
            g.token = token
            return
        raise Exception(introspect_user)  
    except Exception as e:
        print(e, flush=True)
        g.user = None

def validate_request(f):
    @functools.wraps(f)
    def decorated_function(*args, **kwargs):
        getUserInfo()
        if g.user is None:
            abort(403)
        return f(*args, **kwargs)
    return decorated_function


@kc_page.route('/isAuthenticated', methods=['GET'])
@validate_request
def is_authenticated():
    return Response(g.user.get("name"), status=200)


@kc_page.route('/login', methods=['POST'])
def login():
    try:
        json_body = request.json
        username= json_body.get('username')
        password= json_body.get('password')
        token = kc.token(username, password)
        return jsonify({"data": { "access_token": token.get("access_token"), 
        "refresh_token":token.get("refresh_token") }})
    except:
        return jsonify({"data":"Unauthorized"})


@kc_page.route("/logout", methods=["GET"]) 
@validate_request
def logout():
    kc.logout(request.headers['Refresh-Token'])
    return Response('Done', status=200)




