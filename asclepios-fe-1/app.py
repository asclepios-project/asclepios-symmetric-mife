# -*- coding: utf-8 -*-
import json
import logging
import operator
import re
import subprocess
import sys
from hashlib import sha256
from random import randrange

from flask import Flask
# from flask_caching import Cache
from flask_restful import Api

from ev_util import EvCiphertext, EvCompute, EvDelete, EvResults
from keycloak_util import kc_page
from parameters import debug, dev, server_port, service
from ta_util import TaCompute, TaDelete, TaKeygenerator

app = Flask(__name__)
app.config['SECRET_KEY'] = '37af2c72-53c3-4d8e-bae0-609a5a2934f6'
api = Api(app)

@app.after_request
def add_header(response):
    if dev:
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Headers'] = '*'
    return response

if service == "ta":
    api.add_resource(TaKeygenerator, '/get-keys')
    api.add_resource(TaCompute, '/compute')
    api.add_resource(TaDelete, '/delete')
    app.register_blueprint(kc_page)
elif service == "ev":
    api.add_resource(EvResults, '/get-result')
    api.add_resource(EvCiphertext, '/ciphertexts')

    # access from TA service
    api.add_resource(EvCompute, '/compute')
    api.add_resource(EvDelete, '/delete')
else:
    exit("Non exist service:" + str(service))
    

if __name__ == '__main__':
    app.run(host= '0.0.0.0',port=server_port,debug=debug)

