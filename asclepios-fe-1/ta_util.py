# -*- coding: utf-8 -*-
import json
import logging
import operator
import re
import subprocess
import sys
from hashlib import sha256
from random import randrange
from uuid import uuid4

import requests
from flask import Flask, g
from flask_restful import Api, Resource, request

from abac_util import permitCheck
from keycloak_util import getUserInfo, kc_page
from parameters import evaluator_delete_url, evaluator_url, server_port
from util import Ciphertexts, MyAES, create_json_response, input_validation

logging.basicConfig(filename='api-logs.log', level=logging.INFO,format='%(asctime)s - %(levelname)s - %(message)s')
logger = logging.getLogger("main")
conlog = logging.StreamHandler()
logger.addHandler(conlog)
logger.info("TA API service started.")



class TaKeygenerator(Resource):


    def __init__(self):
        getUserInfo()

    def __generate_fe_keys__(self, fieldWithIDs):
        
            fe_keys = {}
            all_fe_keys = Ciphertexts(email=g.user.get("email", ""))
            logger.error("test")
            aes = MyAES()
            logger.error("test")
            logger.error("fieldWithIDs")
            logger.error(fieldWithIDs)
            for fieldWithID in fieldWithIDs:
                try:
                    if not input_validation(fieldWithID) or all_fe_keys.exist(fieldWithID):
                        continue
                    fe_keys[fieldWithID]= randrange(9999999) + 100000
                    all_fe_keys.add(id=fieldWithID,  ciphertext=aes.encrypt(str(fe_keys[fieldWithID])))
                except Exception as e:
                    logger.error(e)
            all_fe_keys.commit()
            all_fe_keys.close()
            print("fe_keys:{}".format(fe_keys))
            return fe_keys


    def post(self):
        try:
            if g.user is None:
                return create_json_response(403,'Unauthenticated request')
            if not permitCheck(request.environ, g.user):
                return create_json_response(403,'Unauthorized request')
            logger.error(request)
            logger.error(request.json)
            json_body = request.json
            total_fe_keys = {}
            total_fileIDs_exists =[]
            fe_keys = self.__generate_fe_keys__(json_body)
            total_exists=(list(set(json_body) - set(fe_keys.keys())))
            return create_json_response(200, {"feKeys": fe_keys, "fileIDsExists": total_exists}, rename_to="results")
        except Exception as e:
            logger.error(e)
            return create_json_response(500,'Internal Error')

class TaCompute(Resource):

    ops = { "sum": operator.add, "subtract": operator.sub }

    def __init__(self):
        getUserInfo()

    def __generate_master_fe_keys__(self,file_ids, fields, mode):
        master_fe_key = None
        aes = MyAES()
        all_fe_keys = Ciphertexts(email=g.user.get("email", ""))
        current_op = self.ops.get(mode, operator.add)
        for file_id in file_ids:
            if not input_validation(file_id):
                continue
            for field in fields:
                if not input_validation(field):
                    continue
                mapping_key = "{}@{}".format(file_id,field)
                logger.error(mapping_key)
                try:
                    logger.error("__generate_master_fe_keys__:master_fe_key{}".format(master_fe_key))
                    if master_fe_key is None:
                        master_fe_key = int(aes.decrypt(all_fe_keys.select(mapping_key)))
                    else:
                        master_fe_key = current_op(master_fe_key, int(aes.decrypt(all_fe_keys.select(mapping_key))))
                except Exception as e:
                    logger.error("__generate_master_fe_keys__:error:{}".format(e))
                    raise FileExistsError(str(e))
        logger.error("master_fe_key:{}".format(master_fe_key))
        return master_fe_key or 0

    def post(self):
        try:
            if g.user is None:
                return create_json_response(403,'Unauthenticated request')
            if not permitCheck(request.environ, g.user):
                return create_json_response(403,'Unauthorized request')
            json_body = request.json
            file_ids = json_body.get('fileIDs')
            logger.error(file_ids)
            field = json_body.get('field')
            operation = json_body.get('function')
            if operation not in ('sum', 'average', 'subtract'):
                return create_json_response(400,'Invalid operation, only supports sum, average, subtract')
            master_fe_key = []
            fields = field.split(",")
            logger.error("fields:{}".format(fields))
            if operation == "subtract" and not ((len(fields) == 2 and len(file_ids) > 0 ) or ( len(fields) == 1 and len(file_ids) == 2 ) ):
                return create_json_response(400,'Subtract needs at least 1 file ID and 2 fields  or  2 file IDs and 1 field')

            if  operation != "subtract" and len(file_ids) < 2:
                logger.error(file_ids)
                return create_json_response(404, "Please provide 2 or more fileIDs")
            try:
                if operation == "subtract" and len(fields) == 2:
                    for file_id in file_ids:
                        master_fe_key.append(self.__generate_master_fe_keys__([file_id], fields, operation))
                else:
                    master_fe_key = [self.__generate_master_fe_keys__(file_ids, fields, operation)]
            except FileExistsError as e:
                return create_json_response(404, "Some of the fields or fileIDs not found")
            logger.error("master_fe_key:{}".format(master_fe_key))     
            
            token = str(uuid4())
            resp = None
            try:
                timeout = 120
                async_request = json_body.get('async')
                if async_request and async_request == "true":
                    timeout = 0.0000000001
                resp = requests.post(
                evaluator_url, 
                json={"mfeks":master_fe_key, "function": operation,"fileIDs": file_ids, "field": field, "token":token },
                timeout=timeout)
            except requests.exceptions.ReadTimeout: 
                pass
            logger.error("response.content:{mfek}-{function}-{fileIDs}-{field}".format(**{"mfek":master_fe_key, "function": operation,"fileIDs": file_ids, "field": field }))
            # resp = resp.json()
            # token = resp.get("token", "")
            return create_json_response(200, token, rename_to="token")
        except Exception as e:
            logger.error(e) 
            logger.error(request.data)
            return create_json_response(500,'Internal Error')

    def generateMFKAndSendToEv(self, file_ids, field, operation):
        master_fe_key = self.__generate_master_fe_keys__(file_ids, field)
        resp = requests.post(
            evaluator_url, 
            json={"mfek":master_fe_key, "function": operation,"fileIDs": file_ids, "field": field }
        )
        logger.error("response.content:{mfek}-{function}-{fileIDs}-{field}".format(**{"mfek":master_fe_key, "function": operation,"fileIDs": file_ids, "field": field }))
        logger.error("response.content:{}".format(resp.content))
        resp = resp.json()
        token = resp.get("token")
        return token


class TaDelete(Resource):
    
    def __init__(self):
        getUserInfo()

    def __remove_keys__(self,file_ids):
        all_fe_keys = Ciphertexts(email=g.user.get("email", ""))
        deleted_list = []
        for file_id in file_ids:
            if all_fe_keys.exist(file_id, authenticate=True):
                deleted_list.append(file_id);
            if "@" in file_id:
                all_fe_keys.delete("{}".format(file_id))
            else:
                all_fe_keys.delete("{}@".format(file_id))
        all_fe_keys.finalize()
        return deleted_list


    def __remove_values_from_EV__(self,file_ids):
        # all_fe_keys = Ciphertexts()
        resp = requests.post(
            evaluator_delete_url, 
            json={"fileIDs": file_ids, "email": g.user.get("email", "")}
        )
        logger.error("response.content:{}".format(resp.content))
        resp = resp.json()

    def post(self):
        try:
            if g.user is None:
                return create_json_response(403,'Unauthenticated request')
            if not permitCheck(request.environ, g.user):
                return create_json_response(403,'Unauthorized request')
            json_body = request.json
            file_ids = json_body.get('fileIDs')
            deleted_list = self.__remove_keys__(file_ids)
            self.__remove_values_from_EV__(file_ids)
            return create_json_response(200, deleted_list, rename_to="deletedList")
        except Exception as e:
            logger.error(e) 
            return create_json_response(500,'Internal Error')

