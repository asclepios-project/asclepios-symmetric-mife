import json
import logging
import re
import subprocess
import sys
from random import randrange

from flask import Flask
# from flask_caching import Cache
from flask_restful import Api, Resource, request

import util
from abac_util import permitCheck
from ev_util import EvCiphertext, EvCompute, EvDelete, EvResults
from keycloak_util import getUserInfo
from parameters import debug, dev, server_port
from util import Ciphertexts, create_json_response, input_validation

app = Flask(__name__)
app.config['SECRET_KEY'] = '37af2c72-53c3-4d8e-bae0-609a5a2934f6'
api = Api(app)

@app.after_request
def add_header(response):
    if dev:
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Headers'] = '*'
    return response


api.add_resource(EvResults, '/get-result')
api.add_resource(EvCiphertext, '/ciphertexts')

# access from TA service
api.add_resource(EvCompute, '/compute')
api.add_resource(EvDelete, '/delete')


if __name__ == '__main__':
    app.run(host= '0.0.0.0',port=server_port,debug=debug)

