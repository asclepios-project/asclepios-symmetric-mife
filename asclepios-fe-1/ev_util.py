import json
import logging
import re
import subprocess
import sys
from random import randrange

from flask import g
from flask_restful import Resource, request
# from flask_caching import Cache
from redis import StrictRedis

import util
from abac_util import permitCheck
from keycloak_util import getUserInfo
from parameters import server_port
from util import Ciphertexts, create_json_response, input_validation

# from redis_cache import RedisCache


# temporary_tokens = Cache(app.app_context(), config={'CACHE_TYPE': 'redis', 'CACHE_REDIS_URL': 'redis://redis-fe:6379/0'})
# client = StrictRedis(host="redis://redis-fe:6379/0", decode_responses=True)
# temporary_tokens = RedisCache(redis_client=client)
temporary_tokens = StrictRedis(host='redis-fe', port=6379, db=0)

logging.basicConfig(filename='api-logs.log', level=logging.INFO,format='%(asctime)s - %(levelname)s - %(message)s')
logger = logging.getLogger("main")
conlog = logging.StreamHandler()
logger.addHandler(conlog)
logger.info("EV service started.")


class EvCiphertext(Resource):
        
    def __init__(self):
        getUserInfo()

    def __store_ciphertext__(self, ciphertexts):
        all_ciphertexts = Ciphertexts(email = g.user.get('email', ""))
        logger.error(ciphertexts)
        for mapping_key, ciphertext in ciphertexts.items():
            # mapping_key = "{}@{}".format(file_id,field)
            if not input_validation(mapping_key):
                continue
            try:
                float(ciphertext)
            except:
                continue
            all_ciphertexts.add(mapping_key, ciphertext)
            logger.error(mapping_key)
            logger.error(ciphertext)
        all_ciphertexts.commit()
        all_ciphertexts.close()
            # EvCiphertext.all_ciphertexts[mapping_key]= ciphertext

    def post(self):
        try:
            if g.user is None:
                return create_json_response(403,'Unauthenticated request')
            if not permitCheck(request.environ, g.user):
                return create_json_response(403,'Unauthorized request')
            ciphertexts = request.json
            self.__store_ciphertext__(ciphertexts)
            # logger.error("fe_keys:{}, file_id:{}, fields:{}".format(fe_keys, file_id, fields))
            return create_json_response(200, "done")
        except Exception as e:
            logger.error(e)
            return create_json_response(500,'Internal Error')



class EvCompute(Resource):

    tokenize_results = {}

    def __get_ciphertexts__(self, file_ids, fields):
        ciphertexts = []
        # logger.error(EvCiphertext.all_ciphertexts)
        logger.error("EvCompute.__get_ciphertexts__")
        logger.handlers[0].flush()
        all_ciphertexts = Ciphertexts()
        logger.error(all_ciphertexts)
        logger.handlers[0].flush()
        logger.error(file_ids)
        logger.handlers[0].flush()
        for file_id in file_ids:
            if not input_validation(file_id):
                continue
            for field in fields:
                if not input_validation(field):
                    continue
                mapping_key = "{}@{}".format(file_id,field)
                # ciphertexts.append(EvCiphertext.all_ciphertexts.get(mapping_key, 0))
                try:
                    ciphertexts.append(round(float(all_ciphertexts.select(mapping_key)),2))
                except Exception as e:
                    logger.error("__get_ciphertexts__:{}".format(e))
        return ciphertexts
            

    def __calculate_sum__(self, mfeks, ciphertexts, file_ids=None, fields=None):
        logger.error("__calculate_sum__ {}-{}".format(mfeks,ciphertexts))
        ciphertexts_sum = 0
        if len(mfeks) != 1:
            return 0
        mfek = mfeks.pop(0)
        for ciphertext in ciphertexts:
            ciphertexts_sum += ciphertext
        logger.error("__calculate_average__:mfek:{}".format(mfek))
        return ciphertexts_sum-mfek if mfek else 0
    
    def __calculate_average__(self, mfeks, ciphertexts, file_ids=None, fields=None):
        logger.error("__calculate_average__{}-{}".format(mfeks,ciphertexts))
        ciphertexts_values = 0
        if len(mfeks) != 1:
            return 0
        mfek = mfeks.pop(0)
        for ciphertext in ciphertexts:
            logger.error("__calculate_average__{}".format(ciphertext))
            ciphertexts_values += ciphertext
        logger.error("__calculate_average__:mfek:{}".format(mfek))
        return (ciphertexts_values-mfek) / len(ciphertexts) if mfek and len(ciphertexts) > 0 else 0

    def __calculate_subtract__(self, mfeks, ciphertexts, file_ids=None, fields=None):
        logger.error("__calculate_subtract__{}-{}".format(mfeks,ciphertexts))
        decrypted_values = []
        for mfek in mfeks:
            try:
                ciphertexts_values = ciphertexts.pop(0) - ciphertexts.pop(0)
                decrypted_values.append(ciphertexts_values-mfek)
            except Exception as e:
                logger.error("__calculate_subtract__:{}".format(e))
        return decrypted_values

    def post(self):
        try:
            json_body = request.json
            mfeks= json_body.get('mfeks')
            operation = json_body.get('function')
            token = json_body.get('token')
            if operation not in ("sum", "average", "subtract"):
                return create_json_response(200, token, rename_to="token")
            file_ids = json_body.get('fileIDs')
            fields = json_body.get('field').split(",")
            logger.error(file_ids)
            logger.error(fields)
            if  operation != "subtract" and len(file_ids) < 2:
                logger.error(file_ids)
                return create_json_response(404, "Please provide 2 or more fileIDs")
            ciphertexts = self.__get_ciphertexts__(file_ids, fields)
            logger.error("ciphertext:{}".format(ciphertexts))
            if not ciphertexts or len(ciphertexts) == 0:
                results = 0
            else: 
                results = self.__getattribute__("__calculate_{}__".format(operation))(mfeks, ciphertexts, file_ids, fields)
            results=json.dumps(results)
            logger.error(results)
            logger.error(token)
            temporary_tokens.set(token, results, ex=100)
            # EvCompute.tokenize_results[token] = results
            return create_json_response(200, token, rename_to="token")
        except Exception as e:
            import sys
            import traceback
            traceback.print_exc(file=sys.stdout)
            logger.error(e) 
            logger.error(request.data)
            return  create_json_response(500,'Internal Error')


class EvResults(Resource):


    def __init__(self):
        getUserInfo()

    def __get_results__(self, token):
        logger.error("__get_results__")
        value = temporary_tokens.get(token)
        logger.error(value)
        if value:
        # return  EvCompute.tokenize_results.pop(token, 0)
            temporary_tokens.delete(token)
            return json.loads(value)
        else:
            return "Computation is in progress"

    def post(self):
        try:
            if g.user is None:
                return create_json_response(403,'Unauthenticated request')
            if not permitCheck(request.environ, g.user):
                return create_json_response(403,'Unauthorized request')
            json_body = request.json
            token = json_body.get('token')
            logger.error("EvResults.post.token:{}".format(token))
            return create_json_response(200, self.__get_results__(token), rename_to="results")
        except Exception as e:
            logger.error(e) 
            logger.error(request.data)
            return create_json_response(500,'Internal Error')


class EvDelete(Resource):

    email = ""

    def __remove_values__(self,file_ids):
        all_values = Ciphertexts(email=self.email)
        for file_id in file_ids:
            if "@" in file_id:
                all_values.delete("{}".format(file_id))
            else:
                all_values.delete("{}@".format(file_id))
        all_values.finalize()


    def post(self):
        try:
            json_body = request.json
            file_ids = json_body.get('fileIDs')
            self.email = json_body.get('email', "")
            self.__remove_values__(file_ids)
            return create_json_response(200, "Done")
        except Exception as e:
            logger.error(e)
            return create_json_response(400,'Something went wrong!!!')


