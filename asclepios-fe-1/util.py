import base64
import errno
import json
import logging
import os
import re
import sqlite3
from collections import namedtuple
from datetime import datetime

from flask_restful import request
import requests
from Crypto import Random
from Crypto.Cipher import AES
from flask import Response

import parameters

logger = logging.getLogger("main")

from string import ascii_lowercase, ascii_uppercase


def fancy_number():
    t=[]
    t.append(ascii_lowercase[1])
    t.append(ascii_uppercase[10])
    t.append(ascii_lowercase[2])
    t.append(str(2))
    t.append(ascii_uppercase[13])
    t.append(ascii_uppercase[11])
    t.append(ascii_lowercase[10])
    t.append(ascii_lowercase[19])
    t.append(str(4))
    t.append(ascii_lowercase[5])
    t.append(ascii_uppercase[6])
    t.append(ascii_lowercase[9])
    t.append(str(8))
    t.append(ascii_uppercase[4])
    t.append(ascii_lowercase[6])
    t.append(ascii_uppercase[16])
    return bytes(''.join(t), 'utf-8')

def input_validation(value):
    if re.match(r"\w+\#?", value):
        return True
    return False


def create_json_response(http_code, message, rename_to=None, action_response_json = {}):
    data = {
            'code' : http_code,
            'message'  : message 
        }
    if rename_to:
        data[rename_to] = data.pop("message")

    data.update(action_response_json)   
    js = json.dumps(data)
    resp = Response(js, status=http_code, mimetype='application/json')
    log = { 
            "type": "response",
            "method": request.method,
            "statusCode": http_code,
            "req": {
                "url": request.url,
                "method": request.method,
                 "headers": {
                    "host": request.host,
                    "user-agent": str(request.user_agent),
                    "accept": str(request.accept_charsets),
                    "accept-language": str(request.accept_languages),
                    "accept-encoding": str(request.accept_encodings),
                    "referer": str(request.referrer),
                    "content-type": str(request.content_type),
                    "origin": str(request.origin),
                    "content-length": str(request.content_length),
                    "connection": str(request.headers.get("connection")),
                    "remote_addr": str(request.remote_addr),
                    "remote_user": str(request.remote_user),
                },
            },
            "res": { 
                "statusCode": http_code, 
                "contentLength": resp.content_length 
            },
            "message": str(message)
    }
    logger.info(json.dumps(log))
    return resp




class Ciphertexts(object):

    def __init__(self, email=""):
        self.email = email
        logger.error("Keys database init")
        self.conn = sqlite3.connect(parameters.db_name)
        logger.error("Keys database init")
        self.c = self.conn.cursor()
        logger.error("Keys database init")
        self.c.execute('''CREATE TABLE IF NOT EXISTS keys
             (id text NOT NULL UNIQUE, ciphertext text, email text)''')
        logger.error("Keys database init")
        self.reset()
        logger.error("Keys database init")

    def reset(self):
        self.keys = []

    def add(self, id, ciphertext):
        self.keys.append((id, ciphertext, self.email,))

    def commit(self):
        rowcount = 0;
        count_keys = len(self.keys) 
        if count_keys > 0:
            rowcount=self.c.executemany('INSERT INTO keys VALUES (?,?,?)', self.keys).rowcount;
            print("rowcount:{}".format(rowcount))
            self.conn.commit()
            self.reset()
        print("count_keys:{}-rowcount:{}".format(count_keys,rowcount))
        return 1 if count_keys == rowcount else 0

    def select(self, id):
        t = (id,)
        self.c.execute('SELECT ciphertext FROM keys WHERE id=?', t)
        row = self.c.fetchone()
        if row and len(row):
            return row[0]
        raise FileNotFoundError(id)

    def insert(self, id, ciphertext):
        try:
            self.c.execute('INSERT INTO keys VALUES (?,?,?)', (id, ciphertext, self.email,))
            return True
        except Exception as e:
            logger.error(e)
            return False
            
    def delete(self, id):
        logger.error("delete:")
        logger.error(self.email)
        self.c.execute("DELETE FROM keys WHERE id LIKE ? AND email LIKE ?", 
        [ '{}%'.format(id), '%{}'.format(self.email)])
        
    def exist(self, id, authenticate=False):
        print("dsjfksdjf")
        if authenticate is True:
            self.c.execute("SELECT EXISTS(SELECT 1 FROM keys WHERE id LIKE ? AND email LIKE ? )",
             [ '{}%'.format(id), '%{}'.format(self.email)])
        else:
            print("dsadsd")
            self.c.execute("SELECT EXISTS(SELECT 1 FROM keys WHERE id LIKE ?)", ["{}%".format(id)])
            # self.c.execute("SELECT * FROM keys WHERE id=?;", [id])
        results = self.c.fetchone()
        print(results)
        logger.error(results)
        if results and results[0]:
            return True
        return False

    def finalize(self):
        self.conn.commit()


    def close(self):
        self.conn.close()


BS = 16
pad = lambda s: bytes(s + (BS - len(s) % BS) * chr(BS - len(s) % BS), 'utf-8')
unpad = lambda s : s[0:-ord(s[-1:])]

class MyAES(object):

    def __init__(self):
        pass

    def encrypt( self, raw ):
        raw = pad(raw)
        iv = Random.new().read( AES.block_size )
        cipher = AES.new(fancy_number(), AES.MODE_CBC, iv )
        return base64.b64encode( iv + cipher.encrypt( raw ) )

    def decrypt( self, enc ):
        enc = base64.b64decode(enc)
        iv = enc[:16]
        cipher = AES.new(fancy_number(), AES.MODE_CBC, iv )
        return unpad(cipher.decrypt( enc[16:] )).decode('utf8')

