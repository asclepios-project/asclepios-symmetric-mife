#!/bin/bash 
kill -9 `pgrep -f "ta.py"`; 
kill -9 `pgrep -f "ev.py"`; 
rm ta_ciphertexts.db
rm ev_ciphertexts.db
rm -rf venv
python3 -m venv venv

source venv/bin/activate && pip3 install -r requirements.txt && export API_PORT=5000 && export EV_URL=localhost:5001 && export DB_NAME=ta_ciphertexts.db && venv/bin/python3 ta.py  > /dev/null 2>&1 &
source venv/bin/activate && pip3 install -r requirements.txt && export API_PORT=5001 && export DB_NAME=ev_ciphertexts.db &&  venv/bin/python3 ev.py  > /dev/null 2>&1 &

wait=1
while [ $wait -ne 0 ]
do
    wait=0
    pgrep -f "ev.py"
    wait=$((wait+$?))
    pgrep -f "ta.py"
    wait=$((wait+$?))
done
sleep 2
clear
echo ""
echo  "FE Sum Encrypt/Decrypt - TA generates the fe-keys"
echo ""
echo -e "
# TA - run in separate terminal
> source venv/bin/activate && pip3 install -r requirements.txt && export API_PORT=5000 && export DB_NAME=ta_ciphertexts.db && venv/bin/python3 ta.py 

# Evaluator - run in separate terminal
> source venv/bin/activate && pip3 install -r requirements.txt && export API_PORT=5001 && export DB_NAME=ev_ciphertexts.db &&  venv/bin/python3 ev.py
"

echo ""

analyst_list=()
first=1
arguments=$@
index=0
for argument in $@
do
    readarray -d : -t splitargument <<< "$argument"
    value=12
    echo ""
    echo "-----------------------------------------"
    echo "Client "$(($index+1))"  fileID: ${splitargument[0]}  age:${splitargument[1]}"
    echo "-----------------------------------------"
    echo ""
    echo ""
    echo "Request Key:"
    # [{"fields":["age"],"fileID":111},{"fields":["age"],"fileID":444}]
    key=`curl -s  -d  '["'${splitargument[0]}'@age"]' -H 'Content-Type: application/json' http://localhost:5000/get-keys  -X POST | jq '.results.feKeys | values[]'`
    echo -e "> curl -s  -d  '[\"${splitargument[0]}@age\"]' -H 'Content-Type: application/json' http://localhost:5000/get-keys  -X POST"
    value=$(( $key + ${splitargument[1]}));
    echo $key
    echo ""
    echo "Submit ciphertext:"
     curl  -so /dev/null -d '{
        "'${splitargument[0]}'@age":"'${value}'"
    }' -H 'Content-Type: application/json'  http://localhost:5001/ciphertexts  -X POST
    echo -e "> curl -d '{
        \"${splitargument[0]}@age\":\"${value}\"
    }'  -H 'Content-Type: application/json'  http://localhost:5001/ciphertexts  -X POST"
echo "";
   
    echo "encrypted value"$(($index+1))":${value}"
    analyst_list+=("${splitargument[0]}")
    index=$(($index+1))
done
echo ""

echo ""
echo "-----------------------------------------"
echo "Analyst"
echo "-----------------------------------------"
echo ""
echo "Request from  TA to compute sum"
echo ""
analyst_list=$(IFS=, ; echo "${analyst_list[*]}")
echo -e "> curl -d '{
\"fileIDs\": [${analyst_list}],
\"field\": \"age\",
\"function\": \"sum\"
}' -H 'Content-Type: application/json'  http://localhost:5000/compute  -X POST"
token=`curl  --data-raw  '{
"fileIDs":['${analyst_list}'],
"field": "age",
"function": "sum"
}' -H 'Accept: application/json' -H 'Content-Type: application/json'  http://localhost:5000/compute  -X POST | jq .token`


## Analyst - get result from EV
echo ""
echo "Gets decrypted sum from EV"
echo ""
echo -e "> curl -d '{
\"token\": "$token"
}' -H 'Content-Type: application/json'  http://localhost:5001/get-result  -X POST"
echo ""
echo "Sum decrypted:"
 curl -s -d '{
"token": '${token}'
}' -H 'Content-Type: application/json'  http://localhost:5001/get-result  -X POST | jq .results

