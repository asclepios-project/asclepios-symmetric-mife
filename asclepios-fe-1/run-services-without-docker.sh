#!/bin/bash 
kill -9 `pgrep -f "ta.py"`; 
kill -9 `pgrep -f "ev.py"`; 
rm ta_ciphertexts.db
rm ev_ciphertexts.db
rm -rf venv
python3 -m venv venv

API_PORT1=5010
API_PORT2=5011
if [[ $# -eq 2 ]] ; then
   API_PORT1=$1
   API_PORT2=$2
elif [[  $# -eq 1 ]]; then
    API_PORT1=$1
fi

export EV_URL="localhost:"$API_PORT2
[[ -z "$1" ]] && APU='default' || MyVar="${DEPLOY_ENV}"
source venv/bin/activate && python3 -m pip install --upgrade pip && pip3 install -r requirements.txt && export API_PORT=$API_PORT1 && export DB_NAME=ta_ciphertexts.db && venv/bin/python3 ta.py   &
source venv/bin/activate && python3 -m pip install --upgrade pip && pip3 install -r requirements.txt && export API_PORT=$API_PORT2 && export DB_NAME=ev_ciphertexts.db &&  venv/bin/python3 ev.py   &
wait=1
while [ $wait -ne 0 ]
do
    wait=0
    pgrep -f "ev.py"
    wait=$((wait+$?))
    pgrep -f "ta.py"
    wait=$((wait+$?))
done
sleep 2
clear
echo "Evaluator and TA services are ready!!!"
echo ""
echo "Open via browser the file asclepios-client-analyst.html in order to test the client and analyst."
echo ""
echo "Enter any key in order to stop the services:"
read
kill -9 `pgrep -f "ta.py"`; 
kill -9 `pgrep -f "ev.py"`; 
