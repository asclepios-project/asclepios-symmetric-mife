
import os

""" Configurable parameters """
server_port = os.getenv("API_PORT", "5000")
evaluator_url = "http://{}/compute".format(os.getenv("EV_URL", "localhost:5011"))
keycloak_server_url=os.getenv("KEYCLOAK_SERVER_URL", "https://asclepios-auth.euprojects.net/auth/")
keycloak_client_id=os.getenv("KEYCLOAK_CLIENT_ID", "suite5client")
keycloak_realm_name=os.getenv("KEYCLOAK_REALM", "user-authentication")
keycloak_client_secret_key=os.getenv("KEYCLOAK_SECRET_KEY", "3d732ead-2f78-4859-8830-f29d880c3c55")
abac_url = "{}/checkJsonAccessRequest".format(os.getenv("ABAC_SERVER_HOST", "https://localhost:7071"))
abac_api_key = os.getenv("ABAC_SERVER_API_KEY", "7235687126587231675321756752657236156321765723")
keycloak_issuer = "{}/realms/user-authentication".format(keycloak_server_url)
abac_disabled = str(os.getenv("ABAC_DISABLED","false")).lower() == "true"
evaluator_delete_url = "http://{}/delete".format(os.getenv("EV_URL", "localhost:5011"))
db_name = os.getenv("DB_NAME", "data.db")
service = str(os.getenv("SERVICE", "ta")).lower()
debug = str(os.getenv("DEBUG","false")).lower() == "true"
dev = str(os.getenv("DEV","false")).lower() == "true"
