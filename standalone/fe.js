var feConfig = {
  url_ta: "localhost:5010", //This will be replaced with correct value at runtime at the web server
  url_ev: "localhost:5011", //This will be replaced with correct value at runtime at the web server
};
const globalFileIDs = [];

let currentDATA = {};
const text_truncate = (str, length, ending) => {
  if (length == null) {
    length = 100;
  }
  if (ending == null) {
    ending = "...";
  }
  if (str.length > length) {
    return str.substring(0, length - ending.length) + ending;
  } else {
    return str;
  }
};

const resetNotify = (elementId = "notify-fe") => {
  document.getElementById(elementId).innerHTML = "";
};
const notify = (text, elementId = "notify-fe", truncate = 192) => {
  try {
    document.getElementById(elementId).innerHTML = `${
      document.getElementById(elementId).innerHTML
    }<div class='alert-primary alert'>${text_truncate(text, truncate)}</div>`;
  } catch (error) {
    console.log(text);
  }
};

const notifyAnalyst = (text) => {
  notify(text, "notify-analyst", 512);
};

const informAnalyst = (text) => {
  try {
    document.getElementById("info-analyst").innerHTML = text_truncate(
      text,
      512
    );
  } catch (error) {
    console.log(text);
  }
};

const checkBoxFieldList = [];
const resetCheckBox = () => {
  const checkBoxEl = document.getElementById("fe-checkbox");
  checkBoxEl.innerHTML = "";
  checkBoxFieldList.splice(0, checkBoxFieldList.length);
  document.getElementById("fe-fields").setAttribute("style", "display: none;");
  document
    .getElementById("fe-fields-not-supported")
    .setAttribute("style", "display: true;");
};

const addCheckBox = (field) => {
  const checkBoxEl = document.getElementById("fe-checkbox");
  if (!checkBoxFieldList) {
    checkBoxEl.innerHTML = "";
  }
  if (checkBoxFieldList.includes(field)) {
    return;
  }
  checkBoxEl.innerHTML += checkBoxItemTemplate(sanitizeHTML(field));
  document.getElementById("fe-fields").setAttribute("style", "display: true;");
  document
    .getElementById("fe-fields-not-supported")
    .setAttribute("style", "display: none;");
  checkBoxFieldList.push(field);
};

const getFeFieldChooses = () => {
  const checkedField = new Array();
  for (const field of checkBoxFieldList) {
    const fieldId = `fe-${field}`;
    if (!!document.getElementById(fieldId).checked) checkedField.push(field);
  }
  return checkedField;
};

const hashingMappingKeys = (dataWithIDs) => {
  const encryptedKeys = {};
  for (const key in dataWithIDs) {
    if (Object.hasOwnProperty.call(dataWithIDs, key)) {
      const value = dataWithIDs[key];
      const splitKey = key.split("@");
      const encryptedKey = `${splitKey[0]}@${hash(splitKey[1])}`;
      encryptedKeys[encryptedKey] = value;
    }
  }
  if (!!encryptedKeys && Object.keys(encryptedKeys).length > 0) {
    return encryptedKeys;
  }
  return dataWithIDs;
};

const hashingList = (items) => {
  const encryptedList = [];
  for (const item of items) {
    encryptedList.push(hash(item));
  }
  if (!!encryptedList && encryptedList.length > 0) {
    return encryptedList;
  }
  return items;
};

let feKeysBodyRequest = Array();
let dataWithIDs = {};
const sanitizeHTML = (str) => {
  return str.replace(/[^\w. ]/gi, function (c) {
    return "&#" + c.charCodeAt(0) + ";";
  });
};
const checkBoxItemTemplate = (
  field
) => `<input type="checkbox" id="fe-${field}" name="${field}">
<label for="fe-${field}" id="fe-label-${field}">${field}</label>`;

const buildCheckBoxesFromInputFile = (jsonString) => {
  resetCheckBox();
  try {
    /*
                  [
                      {
                          "age": 30,
                          "heigth": 160,
                          "fileID": 111
                      },
      
                      {
                          "age": 20,
                          "heigth": 165,
                          "fileID": 112
                      }
                  ]
                  */

    const data = JSON.parse(jsonString);
    currentDateFormats = {};
    if (!Array.isArray(data)) {
      loopThroughKeys(data);
      return;
    }
    for (const entry of data) {
      loopThroughKeys(entry);
    }
  } catch (e) {
    notify("Invalid json file");
    console.log("Invalid data" + e);
  }
};
const dateTimeFormatChecker = (entries) => {
  console.log(entries);

  const currentDateFormats = {};
  for (const id in entries) {
    if (id.toLowerCase() === "fileid") continue;
    const currentDateTimeFormat = checkDateTime(entries[id]);
    if (!!currentDateTimeFormat) {
      currentDateFormats[id] = currentDateTimeFormat;
    }
  }
  const dateTimeFormatParentEls = document.getElementsByClassName(
    "fe-datetime-format-update"
  );
  if (!!currentDateFormats && Object.keys(currentDateFormats).length > 0) {
    Object.values(dateTimeFormatParentEls).forEach((element) => {
      element.setAttribute("style", "display:true");
    });
    document.getElementById("fe-datetime-format-update").value = Object.values(
      currentDateFormats
    ).reduce((results, value) => {
      if (!!value) results = value;
      return results;
    }, "");
  } else {
    Object.values(dateTimeFormatParentEls).forEach((element) => {
      element.setAttribute("style", "display:none");
    });
  }
};
const parseInput = (jsonString, checkedField) => {
  try {
    /*
                  [
                      {
                          "age": 30,
                          "heigth": 160,
                          "fileID": 111
                      },
      
                      {
                          "age": 20,
                          "heigth": 165,
                          "fileID": 112
                      }
                  ]
                  */

    let data = JSON.parse(jsonString);
    console.log(data);
    feKeysBodyRequest = Array();
    dataWithIDs = {};
    if (!Array.isArray(data)) {
      data = [data];
    }
    const tmpGlobalFileIDs = globalFileIDs;
    for (const entry of data) {
      console.log(entry);
      const currentFeKeyRowRequestBody = {};
      currentFeKeyRowRequestBody["fields"] = new Array();
      // dataWithIDs[json["fileID"]] = {};
      let fileID = tmpGlobalFileIDs.shift();
      feKeysBodyRequest;
      currentFeKeyRowRequestBody["fileID"] = fileID;
      for (const id in entry) {
        if (id.toLowerCase() === "fileid") continue;
        if (!checkedField.includes(id)) continue;
        currentFeKeyRowRequestBody["fields"].push(id);
        dataWithIDs[`${fileID}@${id}`] = entry[id];

        // dataWithIDs[json["fileID"]][id] = json[id];
      }
      if (
        !!currentFeKeyRowRequestBody["fields"] &&
        !!currentFeKeyRowRequestBody["fields"].length
      ) {
        feKeysBodyRequest.push(currentFeKeyRowRequestBody);
      }
    }
  } catch (e) {
    console.log("Invalid data" + e);
  }
};

// Javascript API Start

const encryptDataAndSendFE = (
  dataWithIDs,
  currentDateTimeFormat,
  encryptNotify
) => {
  fetch(`http://${feConfig.url_ta}/get-keys`, {
    method: "post",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "no-cors",
    },
    body: JSON.stringify(Object.keys(dataWithIDs)),
  })
    .then(async function (response) {
      try {
        const results = (await response.json()).results;
        console.log(results.fileIDsExists);
        console.log(results.feKeys);
        if (
          !!results.fileIDsExists &&
          results.fileIDsExists.length > 0 &&
          !!encryptNotify
        ) {
          encryptNotify(
            `FE fileIDs exists:${JSON.stringify(results.fileIDsExists)}`
          );
        }
        if (!Object.entries(results.feKeys).length) {
          if (!!e) {
            e.preventDefault();
          }
          return;
        }
        const feKeys = results.feKeys;
        const encryptedData = {};
        for (const id in feKeys) {
          try {
            if (feKeys.hasOwnProperty(id)) {
              console.log(id + " -> " + feKeys[id]);
              const currentField = id.split("@")[1];
              // const currentDateTime =
              //   Object.keys(currentDateFormats).indexOf(currentField) >= 0
              //     ? currentDateFormats[currentField]
              //     : null;
              if (
                !Number.isInteger(dataWithIDs[id]) &&
                !!currentDateTimeFormat
              ) {
                const datetimeFormatConv = convertDateTimeToUnixTimestamp(
                  dataWithIDs[id],
                  currentDateTimeFormat
                );
                if (!!datetimeFormatConv) {
                  dataWithIDs[id] = datetimeFormatConv;
                }
              }

              if (!!Number.isInteger(dataWithIDs[id])) {
                encryptedData[id] = dataWithIDs[id] + feKeys[id];
              }

              console.log(`encryptedData[${id}]=${encryptedData[id]}`);
            }
          } catch (e) {
            console.log(e);
          }
        }

        if (!!encryptedData) {
          fetch(`http://${feConfig.url_ev}/ciphertexts`, {
            method: "post",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
            },
            body: JSON.stringify(encryptedData),
          })
            .then(async function (response) {
              console.log(response);
              if (!!encryptNotify) {
                encryptNotify(
                  `FE Ciphertexts:${sanitizeHTML(
                    JSON.stringify(encryptedData)
                  )}`
                );
              }
            })
            .catch(function (error) {
              console.error(error);
            });
        }
        console.log(dataWithIDs);
      } catch (e) {
        console.log(e);
      }
    })
    .catch(function (error) {
      console.error(error);
    });
  // });
};

const computeFE = (fileIDs, field, operator, notifyAnalyst) => {
  // Checks if fields are filled-in or not, returns response "<p>Please enter your details.</p>" if not.
  if (!fileIDs || fileIDs.length === 0 || field == "" || operator == "") {
    if (!!notifyAnalyst) notifyAnalyst(`${operator}=0`);
    return 0;
  }
  field = hashingList(field.split(",")).join(",");
  const computeBodyRequest = {
    fileIDs: fileIDs,
    field,
    function: operator,
  };
  fetch(`http://${feConfig.url_ta}/compute`, {
    method: "post",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(computeBodyRequest),
  })
    .then(async function (response) {
      console.log(response);
      if (response.status >= 200 && response.status < 300) {
        try {
          const token = (await response.json()).token;
          console.log(token);
          fetch(`http://${feConfig.url_ev}/get-result`, {
            method: "post",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
            },
            body: JSON.stringify({ token }),
          })
            .then(async function (response) {
              try {
                const results = (await response.json()).results;
                console.log(results);

                if (!!notifyAnalyst) notifyAnalyst(`${operator}:${results}`);
                return results;
              } catch (e) {
                console.log(e);
              }
            })
            .catch(function (error) {
              console.error(error);
            });
        } catch (e) {
          console.log(e);
        }
      } else {
        if (!!notifyAnalyst) {
          try {
            notifyAnalyst((await response.json()).message);
          } catch (error) {
            notifyAnalyst("Something went wrong!!!");
          }
        }
      }
    })
    .catch(function (error) {
      console.error(error);
    });
};

const feDeleteData = (fileIDs, deleteCallback) => {
  if (!Array.isArray(fileIDs)) {
    fileIDs = [fileIDs];
  }
  if (!!resetNotify) {
    resetNotify("fe-delete-message");
  }
  fetch(`http://${feConfig.url_ta}/delete`, {
    method: "post",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ fileIDs }),
  })
    .then(async function (response) {
      try {
        const results = await response.json();
        console.log(results);
        if (!!deleteCallback) {
          await deleteCallback(results);
        } else if (!!notify) {
          const { deletedList } = results;
          notify(
            `FE Keys/Values deleted list:${deletedList}`,
            "fe-delete-message"
          );
        }
      } catch (e) {
        console.log(e);
      }
    })
    .catch(function (error) {
      console.error(error);
    });
};

const feUpdateData = (jsonObj, file_id, dateTimeFormat, updateNotify) => {
  const keys = Object.keys(jsonObj);
  const values = Object.values(jsonObj);
  const dataWithIDs = {};
  for (let index = 0; index < keys.length; index++) {
    const key = keys[index];
    const value = values[index];
    dataWithIDs[`${file_id}@${hash(key)}`] = value;
  }
  feDeleteData(Object.keys(dataWithIDs), async (results) => {
    const { deletedList } = results;
    if (!!deletedList && deletedList.length > 0) {
      const deletedListWithData = {};
      for (const key in dataWithIDs) {
        if (dataWithIDs.hasOwnProperty(key)) {
          const currentValue = dataWithIDs[key];
          if (deletedList.includes(key)) {
            deletedListWithData[key] = currentValue;
          }
        }
      }
      encryptDataAndSendFE(deletedListWithData, dateTimeFormat, (message) => {
        updateNotify(message);
      });
    }
  });
};

// Javascript API End

(function () {
  document
    .getElementById("fe-analyst-function")
    .addEventListener("change", function (e) {
      if (document.getElementById("fe-analyst-function").value === "subtract") {
        informAnalyst(
          "<b>*Subtract needs at least 1 file ID and 2 fields  or  2 file IDs and 1 field</b>"
        );
      } else {
        informAnalyst("");
      }
    });

  document
    .getElementById("fe-analyst-compute")
    .addEventListener("click", (formsubmission) => {
      const fileIDs = document.getElementById("fe-analyst-fileIDs").value;
      const field = document.getElementById("fe-analyst-field").value;
      const operator = encodeURIComponent(
        document.getElementById("fe-analyst-function").value
      );
      computeFE(
        fileIDs.replace(" ", "").split(","),
        field,
        operator,
        notifyAnalyst
      );
    });
})();
const submitFEData = (e) => {
  resetNotify();
  console.log(currentDATA);
  parseInput(currentDATA, getFeFieldChooses());
  console.log(feKeysBodyRequest);
  console.log(dataWithIDs);
  const currentDateFormat = document.getElementById("fe-datetime-format").value;
  dataWithIDs = hashingMappingKeys(dataWithIDs);
  encryptDataAndSendFE(dataWithIDs, currentDateFormat, notify);
};

const dateTimeFormats = [
  "D-M-Y hh:mm:ss",
  "D/M/Y hh:mm:ss",
  "M-D-Y hh:mm:ss",
  "M/D/Y hh:mm:ss",
  "Y-D-M hh:mm:ss",
  "Y/D/M hh:mm:ss",
  "Y-M-D hh:mm:ss",
  "Y/M/D hh:mm:ss",

  "D-M-Y HH:mm:ss",
  "D/M/Y HH:mm:ss",
  "M-D-Y HH:mm:ss",
  "M/D/Y HH:mm:ss",
  "Y-D-M HH:mm:ss",
  "Y/D/M HH:mm:ss",
  "Y-M-D HH:mm:ss",
  "Y/M/D HH:mm:ss",
];

const convertDateTimeToUnixTimestamp = (stringDateTime, format) => {
  if (!!format) {
    return parseInt(moment(stringDateTime, format, true).format("X"));
  }
  for (const dateTimeFormat of dateTimeFormats) {
    const currentTimestamp = moment(
      stringDateTime,
      dateTimeFormat,
      true
    ).format("X");
    if (!!parseInt(currentTimestamp)) {
      return parseInt(currentTimestamp);
    }
  }
  return;
};

const checkDateTime = (stringDateTime) => {
  for (const dateTimeFormat of dateTimeFormats) {
    const currentTimestamp = moment(
      stringDateTime,
      dateTimeFormat,
      true
    ).format("X");
    if (!!parseInt(currentTimestamp)) {
      return dateTimeFormat;
    }
  }
  return null;
};

const loopThroughKeys = (entry) => {
  console.log(entry);

  const currentDateFormats = {};
  for (const id in entry) {
    if (id.toLowerCase() === "fileid") continue;
    const currentDateTimeFormat = checkDateTime(entry[id]);
    if (Number.isInteger(entry[id]) || !!currentDateTimeFormat) {
      addCheckBox(id);
    }
    if (!!currentDateTimeFormat) {
      currentDateFormats[id] = currentDateTimeFormat;
    }
  }
  const dateTimeFormatEl = document.getElementById("fe-checkbox")
    .nextElementSibling;
  if (!!currentDateFormats && Object.keys(currentDateFormats).length > 0) {
    dateTimeFormatEl.setAttribute("style", "display:true");
    document.getElementById("fe-datetime-format").value = Object.values(
      currentDateFormats
    ).reduce((results, value) => {
      if (!!value) results = value;
      return results;
    }, "");
  } else {
    dateTimeFormatEl.setAttribute("style", "display:none");
  }
};

const currentFileIDs = [];
let retriveFileIDsEnabled = false;
function retriveFileIDs(response) {
  currentFileIDs.splice(0, currentFileIDs.length);
  const listData = response.Cfw;
  for (const row of listData) {
    try {
      currentFileIDs.push(row[0].jsonId);
    } catch (error) {
      console.log(`FE:retriveFileIDs:${e}`);
    }
  }
  return retriveFileIDsEnabled;
}

function feFileIDs() {
  return currentFileIDs;
}

const clearGlobalFileIDs = () => {
  globalFileIDs.splice(0, globalFileIDs.length);
};

const pushGlobalFileIDs = (fileID) => {
  globalFileIDs.push(fileID);
};

const getGlobalFileIDs = () => globalFileIDs;

const updateFeCurrentData = (data) => {
  currentDATA = data;
};
