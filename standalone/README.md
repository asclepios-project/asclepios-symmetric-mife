# ASCLEPIOS Symmetric MIFE | FE stand-alone html version

Implementation of the ASCLEPIOS symmetric multi-input functional encryption scheme.

## Contents

1. [Setup](#Setup)
2. [RUN](#RUN)
   1. [Upload](#upload)
   2. [Compute](#compute)
   3. [Delete](#delete)
   4. [Update](#update)
3. [Javascript API Specification](#javascript-api-specification)
   1. [encryptDataAndSendFE](#encryptDataAndSendFE)
   2. [computeFE](#computeFE)
   3. [feDeleteData](#feDeleteData)
   4. [feUpdateData](#feUpdateData)

## Setup

```bash
   docker-compose -f ../docker-compose.yml down -v  &&  docker-compose -f ../docker-compose.yml  --env-file=env-example  up --build
```

## RUN

Open via browser the [stand-alone-fe.html](./stand-alone-fe.html)

### Upload

Upload fe data using a json or json list as input and please be aware to select any of the check box below in order to enable FE on the specific field

![Upload](./demo-video/upload-data.gif)

### Compute - Analyst

Use as input (1)fileIDs, (2)the field on which the computation should be performed and (3)the type of function to be applied .

![Compute](./demo-video/compute-data.gif)

### Delete

Delete fe data using only the fileID.

![Delete](./demo-video/delete-data.gif)

### Update

Update fe data using a json as input and the fileID on which the new data will be apply

![Update](./demo-video/update-data.gif)

## Javascript API Specification

### encryptDataAndSendFE

Encrypts data localy(on browser) and send the data encrypted. This function accept data of the form `{"<fileId>@<field>":"<unencryptedValue>"}` and encrypts the data with FE keys. FE keys are recieved via request from TA service on the fly. Finally sends the encrypted data along with fileID to EV service.

inputs: dataWithIDs, encryptNotify, currentDateTimeFormat
| Argument | Type | Description | Optional |
| :--- | :---: | :---: | ---: |
| dataWithIDs | json |`{"<fileId>@<field>":"<unencryptedValue>"}` | False |
| encryptNotify | function | called with specific messages: `FE fileIDs exists:` or `FE Ciphertexts:` | True |
| currentDateTimeFormat | string | datetime format, to convert a detected fields into timestamp ( in secs) | True |

output: null

### computeFE

Computes the value of field of a list of fileIDs( at least 2 fileIDs) base on the operation ( sum, average, subtract ). Specific for subtract needs at least 1 file ID and 2 fields or 2 file IDs and 1 field.

inputs: fileIDs, field, operator, notifyAnalyst
| Argument | Type | Description | Optional |
| :--- | :---: | :---: | ---: |
| fileIDs | list | list of file IDs | False |
| field | string | list of fields | False |
| operator | string | supports: sum,average,subtract | False |
| notifyAnalyst | function | called with specific message: `${operator}:${results}` | True |

output: the computation result

### feDeleteData

Deletes encrypted data and key base on fileID.

inputs: fileIDs, deleteCallback
| Argument | Type | Description | Optional |
| :--- | :---: | :---: | ---: |
| fileIDs | list | list of file IDs or combination with fileID and field ex.: `[ "1", "2@age", "2@height"]` | False |
| deleteCallback | function | is called with returned results | True |

output: null

### feUpdateData

Updates encrypted data/key based on specific fileID.

inputs: jsonObj, file_id, updateNotify
| Argument | Type | Description | Optional |
| :--- | :---: | :---: | ---: |
| jsonObj | json | a single json in the form of `{<field>:<value>}` | False |
| file_id | string | a fileID | False |
| dateTimeFormat | string | datetime format, to convert a detected fields into timestamp ( in secs) | True |
| updateNotify | function | is called with a list of updated data | True |

output: null
