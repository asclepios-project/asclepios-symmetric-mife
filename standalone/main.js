/// APPLICATION CONFIGURATION
var appConfig = {
  used_fields: 2, // number of active fields in json_form.html
  all_fields: 24, // total number of fields in json_form.html
};

function hash(input) {
  var bitArray = sjcl.hash.sha256.hash(input);
  var ret = sjcl.codec.hex.fromBits(bitArray);
  return ret;
}

/// APPLICATION CONFIGURATION - End

/// HANDLERS
// Handle event of data upload data
function handleFileLoad(event) {
  var fileIds = $("#fileid1").val().split(",");
  let jsonObj = $("#json-upload").val();

  updateFeCurrentData(jsonObj);
  $("#notify").html("");
  $("#exetime").html("");
  clearGlobalFileIDs();
  jsonObj = JSON.parse(jsonObj);
  if (!!jsonObj && !Array.isArray(jsonObj) && Object.keys(jsonObj).length > 0) {
    jsonObj = [jsonObj];
  }
  for (const jsonItem of jsonObj) {
    let fileId = fileIds.pop();

    if (!fileId && !jsonItem.hasOwnProperty("fileID")) {
      fileId = hash(Math.random().toString(36).substring(7));
    } else if (!fileId && jsonItem.hasOwnProperty("fileID")) {
      fileId = jsonItem.fileID;
    }
    pushGlobalFileIDs(fileId);
  }

  if (getGlobalFileIDs().length > 0) {
    submitFEData();
  }
}

//Handle update data event
function handleUpdateFileLoad(event) {
  var jsonObj = JSON.parse($("#json-update").val());

  var file_id = $("#fileid2").val();
  resetNotify("update-fe-notify");
  if (file_id == "") {
    message = "Please provide file id ";
  } else {
    const updateDataDateFormat = document.getElementById(
      "fe-datetime-format-update"
    ).value;
    feUpdateData(jsonObj, file_id, updateDataDateFormat, (message) =>
      notify(message, "update-fe-notify")
    );
  }

  $("#update").empty();
  $("#updatetime").empty();
}

//Handle update data event
function handleDeleteFile() {
  var file_id = $("#fileid3").val();
  if (file_id == "") {
    message = "Please provide file ";
  } else {
    console.log("Delete data");
    var result = feDeleteData(file_id);
  }
  $("#delete").empty();
  $("#deletetime").empty();
}

/// HANDLERS - END
function prettyPrint(element) {
  try {
    var ugly = element.value;
    var obj = JSON.parse(ugly);
    var pretty = JSON.stringify(obj, undefined, 4);

    element.style = "background-color:lightgreen";
    const curretCursorPosition = element.selectionStart;
    const currentTextSize = element.value.length;
    element.value = pretty;
    const newCursorPosition =
      curretCursorPosition + (pretty.length - currentTextSize);
    element.selectionStart = newCursorPosition;
    element.selectionEnd = newCursorPosition;
  } catch (error) {
    if (!element.value) {
      element.style = "background-color:white";
    } else {
      element.style = "background-color:lightcoral";
    }
  }
}

$(document).ready(function () {
  $("#json-upload").on("input", (e) => {
    prettyPrint(e.target);
    resetNotify();
    buildCheckBoxesFromInputFile(e.target.value);
  });

  $("#json-update").on("input", (e) => {
    prettyPrint(e.target);
    dateTimeFormatChecker(JSON.parse(e.target.value));
  });

  $("#formInput").click(function () {
    $("#notify").empty();
    $("#exetime").empty();
    $("#result").empty();
    $("#searchtime").empty();
  });

  // ADD PATIENT by submitting file
  $("#btnSubmitFile").click(function () {
    $("#notify").empty();
    handleFileLoad();
  });

  /// UPDATE by submitting json file
  $("#btnUpdateFile").click(function (event) {
    $("#resultUpdate").empty();
    handleUpdateFileLoad(event);
  });

  /// DELETE by submitting json file
  $("#btnDeleteFile").click(function () {
    $("#resultDelete").empty();
    handleDeleteFile();
  });
});
