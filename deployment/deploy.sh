#!/bin/sh
VERSION=$1
cat env-example | grep VERSION
[ "$?" = "1" ] && echo "VERSION=${VERSION:-latest}" >> env-example
sed -i -e "s|VERSION=.*|VERSION="${VERSION:-latest}"|g" env-example
echo $VERSION | grep -vq "-sec"
if [ "$?" = "0" ]; then
cat docker-compose.yml | grep -q  "# command";
[ "$?" = "0" ] && sed -i -e "s|# command|command|g" docker-compose.yml
else
echo $VERSION | grep -q "-sec" &&  cat docker-compose.yml | grep -q  "# command"; 
[ "$?" = "1" ] && sed -i -e "s|command:|# command:|g" ./docker-compose.yml
fi

docker-compose --env-file=env-example pull
docker-compose --env-file=env-example up -d
docker-compose --env-file=env-example  logs
docker ps
docker image prune -a -f
docker logout
